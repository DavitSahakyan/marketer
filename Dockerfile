FROM python:3.10

ENV PYTHONUNBUFFERED=1
WORKDIR /app

RUN pip install --upgrade pip 
RUN pip install pipenv

COPY Pipfile Pipfile.lock /app/

RUN pipenv install --system --dev

COPY . /app/

CMD pytest

EXPOSE 8000
