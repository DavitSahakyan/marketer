import os
from celery import Celery

import configurations

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "marketerspace.settings.Dev")
os.environ.setdefault('DJANGO_CONFIGURATION', 'Dev')

configurations.setup()

celery = Celery("marketerspace")
celery.config_from_object("django.conf:settings", namespace="CELERY")
celery.autodiscover_tasks()