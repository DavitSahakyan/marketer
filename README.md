##***Run program with Docker***
```commandline
docker-compose up --build
```
##***SMTP server on port 5000***

##***DESCRIPTION***
The first user and the first organization should be created programmatically. It is the superuser. Users should be connected to Organizations. 
The user can belong to only one organization, but the organization can have many users.
Users can not exist without organization. Users should have the following fields: first name, last name, profile, picture, organization, email, country, creation date, modification date, and other fields if required during development. 
Only email is required. The organization should have the following fields: domain, and name. 
The main user types are the superuser, user (without a specific role), and organization admin. 
Organizations can be created and deleted only by superusers and updated by org admins and superusers. Organization information should be shown only to organization users and superusers. Only a superuser can invite a user, and an org admin can invite a user to only its organization. 
Organization admin can activate and deactivate, as well as delete the user of its org. Superusers can do the same actions with all users. 
The superuser should have an opportunity to invite users (by email) to the organization mentioning user type. When he invites the user, the user object should not be created unless he accepts the invitation.
The email subject should be: Invitation from {first name} {last_name}: Marketer Space Body should be {first name} {last_name} ({email}) has invited you to join {org name} organization: Accept [invitaion link]
The invitation should be sent via email providing an invitation link. The link should expire if it is not accepted within 1 day. The invitation data should be kept in DB. The auth should be done via JWT tokens.

##***SECOND PART...***

As a user, I want to be able to create a marketing campaign. The marketing campaign should have a goal, and a scheduled time when the emails should be sent. The campaign may have the following statuses: Not started, started, paused, and completed. The campaign can be paused and completed after it is started: the transition to paused only can be from “started” status, the transition to completed can be from started and paused. I can upload contacts to the campaign via CSV files (only CSV is supported). The CSV should contain the following columns: first_name, last_name, email, company_name, and job title. I can upload multiple files to each campaign. The uploaded files should be kept in DB: ‘file path’, ‘uploaded at’, and ’uploaded by. Whenever the file is uploaded, the data should be saved in the DB as contacts. The max file size should be 2 MB. Whenever the contact list is removed from DB, all the contacts associated with that list should be deleted. Whenever the contact list upload process is finished, I, as a user should get an email about it. The email should contain information about the actually created contact numbers and the contacts that are not created due to validation (invalid email, or something else). Whenever the campaign is started or completed, I should not be allowed to upload files to that campaign. Whenever the campaign is paused but the scheduled date is in the future, I still can upload files, but if the date is hit, I should not be allowed. Whenever the date is hit, but the campaign status is not started, the emails should not go out. The campaign template is attached to the campaign. It has a subject and content. The subject and content should be set by me. The email should be sent to all contacts attached to that campaign. Each sent email should be stored in the dB (including subject and content). I should have the ability to set {first_name} in the template and this variable should be replaced by the first name of the contact whenever it is out.