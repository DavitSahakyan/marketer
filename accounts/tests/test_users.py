from rest_framework import status
from model_bakery import baker
from accounts.models import User, Organization
import pytest

@pytest.mark.django_db
class TestCreateUser:
    def test_if_post_request_from_accounts_endpoint_returns_405(self, api_client, authenticate):
        authenticate(is_staff=True, is_superuser=True)
        response = api_client.post("/accounts/users/")
        assert response.status_code == status.HTTP_405_METHOD_NOT_ALLOWED


@pytest.mark.django_db
class TestDeleteUser:
    def test_if_anonymous_returns_401(self, api_client): 
        user = baker.make(User)
        response = api_client.delete(f"/accounts/users/{user.id}/")
        assert response.status_code == status.HTTP_401_UNAUTHORIZED


    def test_if_not_superuser_returns_403(self, api_client, authenticate):
        authenticate(is_staff=True)
        user = baker.make(User)
        response = api_client.delete(f"/accounts/users/{user.id}/")
        assert response.status_code == status.HTTP_403_FORBIDDEN


    def test_if_superuser_returns_204(self, api_client, authenticate):
        authenticate(is_staff=True, is_superuser=True)
        user = baker.make(User)
        response = api_client.delete(f"/accounts/users/{user.id}/")
        assert response.status_code == status.HTTP_204_NO_CONTENT


    def test_if_user_does_not_exists_returns_404(self, api_client, authenticate):
        authenticate(is_staff=True, is_superuser=True)
        response = api_client.delete(f"/accounts/users/0/")
        assert response.status_code == status.HTTP_404_NOT_FOUND


@pytest.mark.django_db
class TestUpdateUser:
    def test_if_data_is_valid_returns_200(self, api_client):
        organization = baker.make(Organization)
        user = baker.make(User, organization=organization)
        api_client.force_authenticate(user=user)
        data = {
            "id": user.id,
            "password": user.password,
            "first_name": "microsoft",
            "last_name": "user",
            "organization": organization.id,
            "email": user.email,
            "country": "AM",
        }
        response = api_client.put(f"/accounts/users/{user.id}/", data=data)
        assert response.status_code == status.HTTP_200_OK
    

    def test_if_user_anonymous_returns_401(self, api_client, authenticate):
        organization = baker.make(Organization)
        user = baker.make(User, organization=organization, is_staff=False, is_superuser=False)
        data = {
            "id": user.id,
            "password": user.password,
            "first_name": "microsoft",
            "last_name": "user",
            "organization": organization.id,
            "email": user.email,
            "country": "AM",
            "is_staff": False,
            "is_superuser": False,
        }
        response = api_client.put(f"/accounts/users/{user.id}/", data=data)
        assert response.status_code == status.HTTP_401_UNAUTHORIZED

@pytest.mark.django_db
class TestUsersRetrieve:
    def test_if_valid_list_returns_200(self, api_client, authenticate):
        authenticate()
        response = api_client.get(f"/accounts/users/")
        assert response.status_code == status.HTTP_200_OK

    def test_if_super_returns_200(self, api_client, authenticate):
        user = baker.make(User)
        authenticate(is_staff=True, is_superuser=True)
        response = api_client.get(f"/accounts/users/{user.pk}/")
        assert response.status_code == status.HTTP_200_OK

    def test_if_dont_exists_returns_404(self, api_client, authenticate):
        user = baker.make(User)
        authenticate()
        response = api_client.get(f"/accounts/users/0/")
        assert response.status_code == status.HTTP_404_NOT_FOUND
    
    def test_if_anonymous_returns_401(self, api_client, authenticate):
        user = baker.make(User)
        response = api_client.get(f"/accounts/users/{user.pk}/")
        assert response.status_code == status.HTTP_401_UNAUTHORIZED
    
