from rest_framework import status
from model_bakery import baker
from accounts.models import User, Organization, Invitation
import pytest
import uuid

@pytest.mark.django_db
class TestInvitationCreate:
    def test_if_anonymous_returns_401(self, api_client):
        data = {
            "receiver": "rece",
            "sender": "send",
            "role": "USR",
            "organization": 1,
        }
        response = api_client.post(f"/accounts/invite/", data)
        assert response.status_code == status.HTTP_401_UNAUTHORIZED

    def test_if_valid_returns_201(self, api_client, authenticate):
        organization = baker.make(Organization)
        authenticate(is_staff=True, is_superuser=True)
        data = {
            "receiver": "rece",
            "sender": "send",
            "role": "USR",
            "organization": organization.pk,
        }
        response = api_client.post('/accounts/invite/', data)
        assert response.status_code == status.HTTP_201_CREATED
    
    def test_if_invalid_data_returns_400(self, api_client, authenticate):
        organization = baker.make(Organization)
        authenticate(is_staff=True, is_superuser=True)
        data = {
            "receiver": "rece",
            "sender": "send",
            "role": "USR",
            "organization": 0,
        }
        response = api_client.post('/accounts/invite/', data)
        assert response.status_code == status.HTTP_400_BAD_REQUEST
    