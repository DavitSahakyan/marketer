from rest_framework import status
import pytest
from model_bakery import baker
from accounts.models import User, Organization

@pytest.fixture
def create_organization(api_client):
    def do_create_organization(organizaton):
       return api_client.post('/accounts/organizations/', organizaton)
    return do_create_organization


@pytest.mark.django_db
class TestOrganizationCreate:
    def test_if_anonymous_returns_401(self, api_client, create_organization):
        response = create_organization({"domain":"aaa", "name":"nnn"})
        assert response.status_code == status.HTTP_401_UNAUTHORIZED
    
    def test_if_admin_returns_201(self, api_client, create_organization, authenticate):
         authenticate(is_staff=True, is_superuser=True)
         response = create_organization({"domain":"aaa", "name":"nnn"})
         assert response.status_code == status.HTTP_201_CREATED
    
    def test_if_invalid_data_returns_400(self, api_client, create_organization, authenticate):
        authenticate(is_staff=True, is_superuser=True)
        response = create_organization({})
        assert response.status_code == status.HTTP_400_BAD_REQUEST


@pytest.mark.django_db
class TestOrganizationUpdate:
    def test_if_anonymous_returns_401(self, create_organization, api_client):
        organization = baker.make(Organization)
        data = {
            "id": organization.id,
            "domain": organization.domain,
            "name": "other name"
        }
        response = api_client.put(f"/accounts/organizations/{organization.id}/", data=data)
        assert response.status_code == status.HTTP_401_UNAUTHORIZED
    
    def test_if_invalid_data_returns_400(self, create_organization, api_client, authenticate):
        organization = baker.make(Organization)
        authenticate(is_staff=True, is_superuser=True)
        data = {
            "id": organization.id,
            "domain":"",
            "name": organization.name
        }
        response = api_client.put(f"/accounts/organizations/{organization.id}/", data=data)
        assert response.status_code == status.HTTP_400_BAD_REQUEST
    
    def test_if_valid_data_return_200(self, create_organization, api_client, authenticate):
        organization = baker.make(Organization)
        authenticate(is_staff=True, is_superuser=True)
        data = {
            "id": organization.id,
            "domain":"www.other.com",
            "name": organization.name
        }
        response = api_client.put(f"/accounts/organizations/{organization.id}/", data=data)
        assert response.status_code == status.HTTP_200_OK

    def test_if_admin_but_other_org_returns_404(self, create_organization, api_client, authenticate):
        first_organization = baker.make(Organization)
        second_organization = baker.make(Organization)
        user = baker.make(User, organization=first_organization, is_staff=True)
        api_client.force_authenticate(user=user)
        data = {
            "id": first_organization.id,
            "domain":"www.other.com",
            "name": first_organization.name
        }
        response = api_client.put(f"/accounts/organizations/{second_organization.id}/", data=data)
        assert response.status_code == status.HTTP_404_NOT_FOUND
    

@pytest.mark.django_db
class TestOrganizationDelete:
    def test_if_anonymous_returns_401(self, create_organization, api_client):
        organization = baker.make(Organization)
        response = api_client.delete(f"/accounts/organizations/{organization.id}/")
        assert response.status_code == status.HTTP_401_UNAUTHORIZED
    
    def test_if_not_admin_returns_403(self, create_organization, api_client, authenticate):
        authenticate(is_staff=True, is_superuser=False)
        organization = baker.make(Organization)
        response = api_client.delete(f"/accounts/organizations/{organization.id}/")
        assert response.status_code == status.HTTP_403_FORBIDDEN

    def test_if_superuser_returns_204(self, create_organization, api_client, authenticate):
        authenticate(is_staff=True, is_superuser=True)
        organization = baker.make(Organization)
        response = api_client.delete(f"/accounts/organizations/{organization.id}/")
        assert response.status_code == status.HTTP_204_NO_CONTENT

    def test_if_organiation_does_not_exists_returns_404(self, api_client, authenticate):
        authenticate(is_staff=True, is_superuser=True)
        response = api_client.delete(f"/accounts/organizations/1/")
        assert response.status_code == status.HTTP_404_NOT_FOUND

@pytest.mark.django_db
class TestOrganizationsRetrieve:
    def test_if_valid_list_returns_200(self, api_client):
        organization = baker.make(Organization)
        user = baker.make(User, organization=organization)
        api_client.force_authenticate(user=user)
        response = api_client.get(f"/accounts/organizations/")
        assert response.status_code == status.HTTP_200_OK

    def test_if_valid_detail_returns_200(self, api_client):
        organization = baker.make(Organization)
        user = baker.make(User, organization=organization)
        api_client.force_authenticate(user=user)
        response = api_client.get(f"/accounts/organizations/{organization.pk}/")
        assert response.status_code == status.HTTP_200_OK

    def test_if_dont_exists_returns_404(self, api_client, authenticate):
        organization = baker.make(Organization)
        authenticate(is_staff=True, is_superuser=True)
        response = api_client.get(f"/accounts/organizations/0/")
        assert response.status_code == status.HTTP_404_NOT_FOUND
    
    def test_if_anonymous_returns_401(self, api_client, authenticate):
        organization = baker.make(Organization)
        response = api_client.get(f"/accounts/organizations/{organization.pk}/")
        assert response.status_code == status.HTTP_401_UNAUTHORIZED