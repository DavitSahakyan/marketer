from rest_framework import serializers
from django.contrib.auth.models import BaseUserManager
from django.contrib.auth.hashers import make_password
from django.contrib.sites.shortcuts import get_current_site
from django.urls import reverse
from datetime import timedelta, datetime
from django.utils.timezone import make_aware
from accounts.models import User, Organization, Invitation
from accounts.utils import Util
import uuid

class UserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(
        write_only=True,
        required=True,
        style={'input_type': 'password', 'placeholder': 'Password'}
    )
    class Meta:
        model = User
        fields = ('id', 'email', 'organization', 'first_name', 'last_name',
                  'country', 'profile_picture',  'date_joined', 'modification_date', "is_active", "password")
    
    def create(self, validated_data):
        validated_data['password'] = make_password(validated_data.get('password'))
        return super(UserSerializer, self).create(validated_data)

    def update(self, instance, validated_data):
        validated_data['password'] = make_password(validated_data.get('password'))
        return super(UserSerializer, self).update(instance, validated_data)


class OrganizationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Organization
        fields = ('id', 'name', 'domain')

class InvitationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Invitation
        fields = ("receiver", "sender", "organization", "role", "token","creation_date")
    
    def create(self, validated_data):
        request = self.context['request']
        sender = request.user
        if sender.is_superuser == False:
            validated_data["organization"] = sender.organization 
            validated_data["role"] = "USR"
            
        token = uuid.uuid4()
        validated_data["token"] = token
        current_site = get_current_site(request).domain
        relative_link = reverse(viewname="invited-signup")
        absolute_url = "http://"+current_site+relative_link+"?token="+str(token)+"&"+validated_data["role"]+"=True"
        subject = f"Invitation from {sender.first_name} {sender.last_name}"
        message = f'{sender.first_name} {sender.last_name} ({sender.email}) has invited you to join ' \
                f'{validated_data["organization"]} organization:\n' \
                f'Accept {absolute_url}'
        from_email = sender.email
        email_data = {"from_email": from_email,
        "subject": subject,
        "message": message,
        "receiver": validated_data["receiver"]}
                            
        Util.send_email(email_data)

        return super(InvitationSerializer, self).create(validated_data)


class UserCreateSerializer(serializers.ModelSerializer):    
    password = serializers.CharField(
        write_only=True,
        required=True,
        style={'input_type': 'password', 'placeholder': 'Password'}
    )
    class Meta:
        model = User
        fields = ('id', "email", "first_name", 'last_name',
                  "organization", 'country', 'profile_picture',
                  'password', "is_staff", "is_superuser")

    email = serializers.EmailField(read_only=True)
    organization = serializers.PrimaryKeyRelatedField(read_only=True)
    is_staff = serializers.BooleanField(read_only=True)
    is_superuser = serializers.BooleanField(read_only=True)
    def create(self, validated_data):
        validated_data['password'] = make_password(validated_data.get('password'))
        request = self.context['request']
        user = request.user
        token = request.GET.get("token")
        invite_info = Invitation.objects.get(token=token)
        if invite_info.creation_date < make_aware(datetime.now() + timedelta(days=1)):
            validated_data['email'] = invite_info.receiver
            validated_data['organization_id'] = invite_info.organization.pk
            validated_data["first_name"] = request.POST.get("first_name", "")
            validated_data["last_name"] = request.POST.get("last_name", "")
            validated_data["country"] = request.POST.get("country")
            validated_data["profile_picture"] = request.FILES.get("profile_picture")
            validated_data["is_staff"] = request.GET.get("ADM", False)
            validated_data["is_superuser"] = request.GET.get("SPR", False)
            return super(UserCreateSerializer, self).create(validated_data)
        else:
            raise ValueError("Token time is expired") 
    