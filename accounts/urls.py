from rest_framework.routers import DefaultRouter 
from django.urls import path, include
from accounts.views import UserList, OrganizationViewSet, UserDetail, InvetedUserSignUpView, InviteUserView

router = DefaultRouter()
router.register("organizations", OrganizationViewSet, basename="organization")
urlpatterns = [
    path("", include(router.urls)),
    path("users/", UserList.as_view()),
    path("users/<int:pk>/", UserDetail.as_view()),
    path("invite/", InviteUserView.as_view()),
    path("invited-signup/", InvetedUserSignUpView.as_view(), name="invited-signup"),
]