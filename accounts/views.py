from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response
from rest_framework.generics import RetrieveUpdateDestroyAPIView, ListAPIView, CreateAPIView
from rest_framework.permissions import AllowAny, IsAdminUser, IsAuthenticated, SAFE_METHODS
from django.core.mail import send_mail, mail_admins, BadHeaderError
from django.conf import settings
from django.shortcuts import render
from accounts.models import User, Organization, Invitation
from accounts.permissions import IsOrgAdmin, IsSuperUser, IsOrgAdminOrSuperUser
from accounts.serializers import UserSerializer, OrganizationSerializer, InvitationSerializer, UserCreateSerializer

class UserList(ListAPIView):
    def get_queryset(self):
        user = self.request.user
        if user.is_superuser:
            return User.objects.all()
        elif user.is_staff:
            return User.objects.filter(organization=user.organization)
        elif user.is_authenticated:
            return User.objects.filter(id=user.id)
        else:
            return User.objects.all()
    serializer_class = UserSerializer
    permission_classes = [IsAuthenticated,]

class UserDetail(RetrieveUpdateDestroyAPIView):
    serializer_class = UserSerializer
    
    def get_queryset(self):
        user = self.request.user
        if user.is_superuser:
            return User.objects.all()
        elif user.is_staff:
            return User.objects.filter(organization=user.organization)
        elif user.is_authenticated:
            return User.objects.filter(id=user.id)

    def get_permissions(self):
        if self.request.method in SAFE_METHODS:
            return [IsAuthenticated(),]
        elif self.request.method in ["PUT", "PATCH"]:
            return [IsAuthenticated(),]
        elif self.request.method in ["DELETE",]:
            return [IsSuperUser(),]
    
    def destroy(self, request, *args, **kwargs):
        user = self.get_object()
        user.is_active = False
        user.save()
        return Response(data='delete success', status=204)

class OrganizationViewSet(ModelViewSet):
    serializer_class = OrganizationSerializer

    def get_queryset(self):
        user = self.request.user
        if user.is_superuser:
            return Organization.objects.all()
        elif user.is_authenticated:
            return Organization.objects.filter(id=user.organization.id)
    
    def get_permissions(self):
        if self.request.method in SAFE_METHODS:
            return [IsAuthenticated(),]
        elif self.request.method in ["PUT", "PATCH"]:
            return [IsOrgAdmin(),]
        elif self.request.method in ["DELETE", "POST"]:
            return [IsSuperUser(),]
     

class InviteUserView(CreateAPIView):
    serializer_class = InvitationSerializer
    permission_classes = [IsOrgAdminOrSuperUser,]
    def get_queryset(self):
       user = self.request.user
       if user.is_superuser:
            return Invitation.objects.all()
       elif user.is_staff:
            return Invitation.objects.filter(organization=user.organization)


class InvetedUserSignUpView(CreateAPIView):
    serializer_class = UserCreateSerializer
    