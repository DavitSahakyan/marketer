from django.core.validators import FileExtensionValidator, validate_image_file_extension
from django.db import models
from django.contrib.auth.models import AbstractUser
from django.contrib.auth.models import BaseUserManager

class Organization(models.Model):
    name = models.CharField(max_length=255, null=False, blank=False)
    domain = models.CharField(unique=True, max_length=255)

    def __str__(self) -> str:
        return self.name


class User(AbstractUser):
    ARMENIA = "AM"
    UNITED_STATES = "US"
    FRANCE = "FR"
    SPAIN = "ES"

    COUNTRY_CHOICES = [
        (ARMENIA, "Armenia"),
        (UNITED_STATES, "United_States"),
        (FRANCE, "France"),
        (SPAIN, "Spain"),
    ]

    username = None
    profile_picture = models.ImageField(upload_to="images", default="Iker.jpg", blank=True, null=True)
    organization = models.ForeignKey(Organization, on_delete=models.CASCADE, related_name="users")
    email = models.EmailField(unique=True)
    country = models.CharField(
    max_length=2, choices=COUNTRY_CHOICES, default=ARMENIA)
    modification_date = models.DateTimeField(auto_now=True, blank=True, null=True)
    date_joined = models.DateTimeField(auto_now_add=True)
    USERNAME_FIELD ="email"
    REQUIRED_FIELDS = ["organization"]

class Invitation(models.Model):
    USER = "USR"
    ORGANIZATION_ADMIN = "ADM"
    SUPER_USER = "SPR"

    ROLE_CHOICES = [
        (USER, "User"),
        (ORGANIZATION_ADMIN, "Organization_Admin"),
        (SUPER_USER, "Super_User"),
    ]

    sender = models.CharField(max_length=255)
    receiver = models.CharField(max_length=255)
    organization = models.ForeignKey(Organization, on_delete=models.CASCADE, related_name="invitations")
    role = models.CharField(
        max_length=3, choices=ROLE_CHOICES, default=USER)
    token = models.UUIDField(blank=True, null=True)
    creation_date = models.DateTimeField(auto_now_add=True)
    