# Generated by Django 4.1.5 on 2023-02-01 21:16

import campaign.models
import django.core.validators
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='CSVFile',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('file_path', models.FileField(upload_to='files', validators=[campaign.models.validate_file, django.core.validators.FileExtensionValidator(allowed_extensions=['csv'])])),
                ('uploaded_by', models.EmailField(max_length=254)),
                ('uploaded_at', models.DateTimeField(auto_now_add=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Template',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('first_name', models.CharField(blank=True, max_length=64, null=True)),
                ('subject', models.CharField(blank=True, max_length=64, null=True)),
                ('content', models.TextField(blank=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Contact',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('first_name', models.CharField(blank=True, max_length=64, null=True)),
                ('last_name', models.CharField(blank=True, max_length=64, null=True)),
                ('email', models.EmailField(max_length=64, unique=True)),
                ('company_name', models.CharField(blank=True, max_length=64, null=True)),
                ('job_title', models.CharField(blank=True, max_length=64, null=True)),
                ('file', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='contacts', to='campaign.csvfile')),
            ],
        ),
        migrations.CreateModel(
            name='Campaign',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('goal', models.CharField(blank=True, max_length=20, null=True)),
                ('scheduled_time', models.DateTimeField()),
                ('state', models.CharField(choices=[('NS', 'Not_Started'), ('ST', 'Started'), ('PA', 'Paused'), ('CM', 'Completed')], default='NS', max_length=2)),
                ('csv_files', models.ManyToManyField(blank=True, related_name='campaigns', to='campaign.csvfile')),
                ('template', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='campaign.template')),
            ],
        ),
    ]
