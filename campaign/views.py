from django.shortcuts import render
from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response
from rest_framework.generics import UpdateAPIView, ListCreateAPIView, CreateAPIView, RetrieveUpdateDestroyAPIView
from rest_framework.permissions import IsAuthenticated
from campaign.serializers import CSVFileSerializer, CampaignUpdateSerializer, \
                                 TemplateSerializer, ContactSerializer, \
                                 CampaignCreateSerializer, AddContactsBackgroundSerializer
from campaign.models import Campaign, Contact, CSVFile, Template


class CSVFileViewSet(ModelViewSet):
    serializer_class = CSVFileSerializer
    queryset = CSVFile.objects.all()
    permission_classes = [IsAuthenticated,]


class TemplateViewSet(ModelViewSet):
    serializer_class = TemplateSerializer
    queryset = Template.objects.all()
    permission_classes = [IsAuthenticated,]


class ContactViewSet(ModelViewSet):
    serializer_class = ContactSerializer
    queryset = Contact.objects.all()
    permission_classes = [IsAuthenticated,]


class CampaignCreateAPIView(ListCreateAPIView):
    serializer_class = CampaignCreateSerializer
    queryset = Campaign.objects.all()
    permission_classes = [IsAuthenticated,]


class CampaignUpdateAPIView(RetrieveUpdateDestroyAPIView):
    serializer_class = CampaignUpdateSerializer
    queryset = Campaign.objects.all()
    permission_classes = [IsAuthenticated,]


class AddContactsBackgroundViewSet(ModelViewSet):
    serializer_class = AddContactsBackgroundSerializer
    queryset = Contact.objects.all()