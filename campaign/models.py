from django.core.validators import FileExtensionValidator
from django.db import models
from rest_framework.exceptions import ValidationError
from accounts.models import User


def validate_file(file):
    file_size = file.size
    if file_size > 2* 1024 * 1024:
        raise ValidationError("Max size of file is 2 MB")


class CSVFile(models.Model):
    file_path = models.FileField(upload_to='files',
                                validators=[validate_file,
                                            FileExtensionValidator(allowed_extensions=['csv'])],
                                blank=False,
                                null=False)
    uploaded_by = models.EmailField(blank=False, null=False)
    uploaded_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)


class Contact(models.Model):
    first_name = models.CharField(max_length=64, blank=True, null=True)
    last_name = models.CharField(max_length=64, blank=True, null=True)
    email = models.EmailField(max_length=64, unique=True)
    company_name = models.CharField(max_length=64, blank=True, null=True)
    job_title = models.CharField(max_length=64, blank=True, null=True)
    file = models.ForeignKey(CSVFile, on_delete=models.CASCADE, blank=False,
                                     null=False, related_name="contacts")


class Template(models.Model):
    first_name = models.CharField(max_length=64, blank=True, null=True)
    subject = models.CharField(max_length=64, blank=True, null=True)
    content = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.subject


class Campaign(models.Model):
    NOT_STARTED = "NS"
    STARTED = "ST"
    PAUSED = "PA"
    COMPLETED = "CM"
    STATE_CHOICES = [
        (NOT_STARTED, "Not_Started"),
        (STARTED, "Started"),
        (PAUSED, "Paused"),
        (COMPLETED, "Completed")
    ]
    goal = models.CharField(max_length=20, blank=True, null=True)
    scheduled_time = models.DateTimeField()
    state = models.CharField(max_length=2, choices=STATE_CHOICES, default=NOT_STARTED)
    csv_files = models.ManyToManyField(CSVFile, blank=True, related_name="campaigns")
    template = models.ForeignKey(Template, on_delete=models.CASCADE)
