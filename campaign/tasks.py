from time import sleep
from celery import shared_task
from datetime import timedelta, datetime
from django.utils.timezone import make_aware
from django.core.mail import send_mail
from campaign.models import Campaign
from campaign.services import create_contacts_from_csv, find_recipient_emails_from_campaign,\
                              find_recipient_first_names



@shared_task()
def start_campaign():
    for campaign in Campaign.objects.all():
        if campaign.state == "NS" and campaign.scheduled_time > make_aware(datetime.now()):
            campaign.state = "ST"
            campaign.save()


@shared_task()
def create_contacts_send_email(csv_path, file_id, user):
    overall, created, not_created = create_contacts_from_csv(csv_path, file_id)

    message = f"""attempted to create {overall} contacts, 
                  of which {created} created, 
                  and {not_created} not_created
                  """
    
    send_mail(
        subject="Contact creation results",
        message=message,
        from_email="marketerspace@gmail.com",
        recipient_list=[user.email,]
    )

@shared_task()
def send_emails_in_campaign_background():
    for campaign in Campaign.objects.all():
        time_now = make_aware(datetime.now())
        scheduled_time = campaign.scheduled_time
        if scheduled_time < time_now and \
           scheduled_time + timedelta(minutes=15) > time_now:
            if len(campaign.csv_files.all()) > 0 and campaign.template != None:
                recipient_list = find_recipient_emails_from_campaign(campaign, campaign.csv_files.all())
                recipienet_names = find_recipient_first_names(campaign, campaign.csv_files.all())
                for name in recipienet_names:
                    if name == "":
                        name = campaign.template.first_name 
                    subject= name+campaign.template.subject
                    content = campaign.template.content
                    send_mail(
                            subject=subject,
                            message=content,
                            from_email="marketerspace@gmail.com",
                            recipient_list=recipient_list
                        )
                    campaign.state = "CM"
                    campaign.save()

    