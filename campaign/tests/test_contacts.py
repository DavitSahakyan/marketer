from rest_framework import status
import pytest
from model_bakery import baker
from campaign.models import Contact, CSVFile

@pytest.fixture
def create_contact(api_client):
    def do_create_contact(contact):
       return api_client.post('/campaign/contacts/', contact)
    return do_create_contact

@pytest.mark.django_db
class TestContactCreate:
    def test_if_anonymous_returns_401(self, create_contact):
        contact = baker.make(Contact)
        data = {
            "first_name": "Davit",
            "last_name": "Sample subject",
            "email": "mail@gmail.com",
            "company_name": "microsoft",
            "file": contact.file,
            "job_title": "manager"
        }
        response = create_contact(data)
        assert response.status_code == status.HTTP_401_UNAUTHORIZED
    
    def test_if_valid_data_returns_201(self, create_contact, authenticate):
        file = baker.make(CSVFile)
        data = {
            "first_name": "Davit",
            "last_name": "Sample subject",
            "email": "mail@gmail.com",
            "company_name": "microsoft",
            "file": file.pk,
            "job_title": "manager"
        }
        authenticate()
        response = create_contact(data)
        assert response.status_code == status.HTTP_201_CREATED

    def test_if_invalid_data_returns_400(self, create_contact, authenticate):
        file = baker.make(CSVFile)
        data = {
            "first_name": "Davit",
            "last_name": "Sample subject",
            "email": "mail@gmail.com",
            "company_name": "microsoft",
            "file": "",
            "job_title": "manager"
        }
        authenticate()
        response = create_contact(data)
        assert response.status_code == status.HTTP_400_BAD_REQUEST

@pytest.mark.django_db
class TestContactUpdate:
    def test_if_anonymous_returns_401(self, create_contact):
        file = baker.make(CSVFile)
        data = {
            "first_name": "Davit",
            "last_name": "Sample subject",
            "email": "mail@gmail.com",
            "company_name": "microsoft",
            "file": file.pk,
            "job_title": "manager"
        }
        response = create_contact(data)
        assert response.status_code == status.HTTP_401_UNAUTHORIZED
    
    def test_if_valid_returns_200(self, api_client, authenticate):
        file = baker.make(CSVFile)
        contact =baker.make(Contact)
        authenticate()
        data = {
            "id": contact.pk,
            "first_name": "Andy",
            "last_name": "Murray",
            "email": "mail@gmail.com",
            "company_name": "microsoft",
            "file": file.pk,
            "job_title": "tenis"
        }
        response = api_client.put(f"/campaign/contacts/{contact.pk}/", data)
        assert response.status_code == status.HTTP_200_OK
    
    def test_if_dont_exist_returns_404(self, api_client, authenticate):
        file = baker.make(CSVFile)
        authenticate()
        data = {
            "id": 1,
            "first_name": "Andy",
            "last_name": "Murray",
            "email": "mail@gmail.com",
            "company_name": "microsoft",
            "file": file.pk,
            "job_title": "tenis"
        }
        response = api_client.put("/campaign/contacts/1/", data)
        assert response.status_code == status.HTTP_404_NOT_FOUND

    def test_if_invalid_data_returns_400(self, api_client, authenticate):
        file = baker.make(CSVFile)
        contact =baker.make(Contact)
        authenticate()
        data = {
            "id": contact.pk,
            "first_name": "Andy",
            "last_name": "Murray",
            "email": "mail@gmail.com",
            "company_name": "microsoft",
            "file": "",
            "job_title": "tenis"
        }
        response = api_client.put(f"/campaign/contacts/{contact.pk}/", data)
        assert response.status_code == status.HTTP_400_BAD_REQUEST


@pytest.mark.django_db
class TestContactDelete:
    def test_if_anonymous_returns_401(self, api_client):
        contact =baker.make(Contact)
        response = api_client.delete(f"/campaign/contacts/{contact.pk}/")
        assert response.status_code == status.HTTP_401_UNAUTHORIZED
    
    def test_if_valid_returns_204(self, api_client, authenticate):
        contact =baker.make(Contact)
        authenticate()
        response = api_client.delete(f"/campaign/contacts/{contact.pk}/")
        assert response.status_code == status.HTTP_204_NO_CONTENT
    
    def test_if_dont_exists_returns_404(self, api_client, authenticate):
        contact =baker.make(Contact)
        authenticate()
        response = api_client.delete(f"/campaign/contacts/0/")
        assert response.status_code == status.HTTP_404_NOT_FOUND

@pytest.mark.django_db
class TestContactRetrieve:
    def test_if_anonymous_returns_401(self, api_client):
        contact =baker.make(Contact)
        response = api_client.get(f"/campaign/contacts/{contact.pk}/")
        assert response.status_code == status.HTTP_401_UNAUTHORIZED

    def test_if_valid_detail_returns_200(self, api_client, authenticate):
        contact =baker.make(Contact)
        authenticate()
        response = api_client.get(f"/campaign/contacts/{contact.pk}/")
        assert response.status_code == status.HTTP_200_OK
    
    def test_if_valid_list_returns_200(self, api_client, authenticate):
        authenticate()
        response = api_client.get(f"/campaign/contacts/")
        assert response.status_code == status.HTTP_200_OK
    
    def test_if_dont_exist_returns_404(self, api_client, authenticate):
        contact =baker.make(Contact)
        authenticate()
        response = api_client.get(f"/campaign/contacts/{contact.pk}/")
        assert response.status_code == status.HTTP_200_OK
