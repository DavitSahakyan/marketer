from rest_framework import status
import pytest
from model_bakery import baker
from campaign.models import Template

@pytest.fixture
def create_template(api_client):
    def do_create_template(template):
       return api_client.post('/campaign/templates/', template)
    return do_create_template

@pytest.mark.django_db
class TestTemplateCreate:
    def test_if_anonymous_returns_401(self, create_template):
        data = {
            "first_name": "Davit",
            "subject": "Sample subject",
            "content": "Sample content"
        }
        response = create_template(data)
        assert response.status_code == status.HTTP_401_UNAUTHORIZED
    
    def test_if_valid_data_returns_201(self, create_template, authenticate):
        data = {
            "first_name": "Davit",
            "subject": "Sample subject",
            "content": "Sample content"
        }
        authenticate()
        response = create_template(data)
        assert response.status_code == status.HTTP_201_CREATED


@pytest.mark.django_db
class TestTemplateDelete:
    def test_if_anonymous_returns_401(self, api_client, create_template):
        template = baker.make(Template)
        response = api_client.delete(f"/campaign/templates/{template.id}/")
        assert response.status_code == status.HTTP_401_UNAUTHORIZED
    
    def test_if_admin_returns_204(self, api_client, create_template, authenticate):
        authenticate()
        template = baker.make(Template)
        response = api_client.delete(f"/campaign/templates/{template.id}/")
        assert response.status_code == status.HTTP_204_NO_CONTENT
    
    def test_if_template_dont_exist_404(self, api_client, create_template, authenticate):
        authenticate()
        template = baker.make(Template)
        response = api_client.delete(f"/campaign/templates/0/")


@pytest.mark.django_db
class TestTemplateUpdate:
    def test_if_anonymous_returns_401(self, create_template, api_client):
        data = {
            "id":1,
            "first_name": "aaa",
            "subject": "subject",
            "content": "content"
        }
        response = api_client.put(f"/campaign/templates/1/", data=data)
        assert response.status_code == status.HTTP_401_UNAUTHORIZED
    
    def test_if_invalid_data_returns_200(self, create_template, api_client, authenticate):
        authenticate()
        template =baker.make(Template)
        data = {
            "id": template.id,
            "first_name": "sss",
            "subject": "subject",
            "content": "content"
        }
        response = api_client.put(f"/campaign/templates/{template.id}/", data=data)
        assert response.status_code == status.HTTP_200_OK

    def test_if_admin_dont_exist_404(self, create_template, api_client, authenticate):
        authenticate()
        template =baker.make(Template)
        data = {
            "id": template.id,
            "first_name": "sss",
            "subject": "subject",
            "content": "content"
        }
        response = api_client.put(f"/campaign/templates/{template.id+1}/", data=data)
        assert response.status_code == status.HTTP_404_NOT_FOUND

@pytest.mark.django_db
class TestTemplateRetrieve:
    def test_if_valid_list_returns_200(self, api_client, authenticate):
        authenticate()
        response = api_client.get(f"/campaign/templates/")
        assert response.status_code == status.HTTP_200_OK

    def test_if_valid_detail_returns_200(self, api_client, authenticate):
        template = baker.make(Template)
        authenticate()
        response = api_client.get(f"/campaign/templates/{template.pk}/")
        assert response.status_code == status.HTTP_200_OK

    def test_if_dont_exists_returns_404(self, api_client, authenticate):
        template = baker.make(Template)
        authenticate()
        response = api_client.get(f"/campaign/templates/0/")
        assert response.status_code == status.HTTP_404_NOT_FOUND
    
    def test_if_anonymous_returns_401(self, api_client, authenticate):
        template = baker.make(Template)
        response = api_client.get(f"/campaign/templates/{template.pk}/")
        assert response.status_code == status.HTTP_401_UNAUTHORIZED
