from rest_framework import status
import pytest
from model_bakery import baker
from campaign.models import Campaign, CSVFile, Template
from django.core.files.uploadedfile import SimpleUploadedFile

@pytest.fixture
def create_campaign(api_client):
    def do_create_campaign(contact):
       return api_client.post('/campaign/campaigns/', contact)
    return do_create_campaign

@pytest.mark.django_db
class TestCampaignCreate:
    def test_if_anonymous_returns_401(self, create_campaign):
        campaign = baker.make(Campaign)
        template = baker.make(Template) 
        csvfile = baker.make(CSVFile)
        data = {
            "goal": "goal",
            "scheduled_time": campaign.scheduled_time,
            "state": "NS",
            "csv_files": csvfile.pk,
            "template": template.pk
        }
        response = create_campaign(data)
        assert response.status_code == status.HTTP_401_UNAUTHORIZED

    def test_if_valid_returns_201(self, create_campaign, authenticate):
        campaign = baker.make(Campaign)
        template = baker.make(Template) 
        csvfile = baker.make(CSVFile)
        authenticate()
        data = {
            "id": campaign.pk,
            "goal": "goal",
            "scheduled_time": campaign.scheduled_time,
            "state": "NS",
            "csv_files": csvfile.pk,
            "template": template.pk
        }
        response = create_campaign(data)
        assert response.status_code == status.HTTP_201_CREATED
    
    def test_if_invalid_data_returns_400(self, create_campaign, authenticate):
        data = {
            "goal": "goal",
        }
        authenticate()
        response = create_campaign(data)
        assert response.status_code == status.HTTP_400_BAD_REQUEST

@pytest.mark.django_db
class TestCampaignUpdate:
    def test_if_valid_returns_200(self, api_client, authenticate):
        campaign = baker.make(Campaign)
        template = baker.make(Template)
        csvfile = baker.make(CSVFile)
        data = {
            "id": campaign.pk,
            "goal": "goal",
            "scheduled_time": campaign.scheduled_time,
            "state": "NS",
            "csv_files": csvfile.pk,
            "template": template.pk
        }
        authenticate()
        response = api_client.put(f"/campaign/campaigns/{campaign.pk}/", data)
        assert response.status_code == status.HTTP_200_OK
    
    def test_if_dont_exist_returns_404(self, api_client, authenticate):
        data = {
            "id": 1,
            "goal": "goal",
            "state": "NS",
        }
        authenticate()
        response = api_client.put(f"/campaign/campaigns/1/", data)
        assert response.status_code == status.HTTP_404_NOT_FOUND
    
    def test_if_anoymous_returns_401(self, api_client):
        data = {
            "id": 1,
            "goal": "goal",
            "state": "NS",
        }
        response = api_client.put(f"/campaign/campaigns/1/", data)
        assert response.status_code == status.HTTP_401_UNAUTHORIZED
    
    def test_if_invalid_data_returns_400(self, api_client, authenticate):
        campaign = baker.make(Campaign)
        template = baker.make(Template)
        csvfile = baker.make(CSVFile)
        data = {
            "id": campaign.pk,
            "goal": "goal",
            "scheduled_time": "",
            "state": "NS",
            "template": template.pk
        }
        authenticate()
        response = api_client.put(f"/campaign/campaigns/{campaign.pk}/", data)
        assert response.status_code == status.HTTP_400_BAD_REQUEST

@pytest.mark.django_db
class TestCampaignDelete:
    def test_if_valid_returns_204(self, api_client, authenticate):
        campaign = baker.make(Campaign)
        authenticate()
        response = api_client.delete(f"/campaign/campaigns/{campaign.pk}/")
        assert response.status_code == status.HTTP_204_NO_CONTENT
    
    def test_if_dont_exist_returns_404(self, api_client, authenticate):
        campaign = baker.make(Campaign)
        authenticate()
        response = api_client.delete(f"/campaign/campaigns/0/" )
        assert response.status_code == status.HTTP_404_NOT_FOUND
    
    def test_if_anonymous_returns_401(self, api_client, authenticate):
        campaign = baker.make(Campaign)
        response = api_client.delete(f"/campaign/campaigns/{campaign.pk}/" )
        assert response.status_code == status.HTTP_401_UNAUTHORIZED

@pytest.mark.django_db
class TestCampaignRetrieve:
    def test_if_valid_list_returns_200(self, api_client, authenticate):
        authenticate()
        response = api_client.get(f"/campaign/campaigns/")
        assert response.status_code == status.HTTP_200_OK

    def test_if_valid_detail_returns_200(self, api_client, authenticate):
        campaign = baker.make(Campaign)
        authenticate()
        response = api_client.get(f"/campaign/campaigns/{campaign.pk}/")
        assert response.status_code == status.HTTP_200_OK

    def test_if_dont_exists_returns_404(self, api_client, authenticate):
        campaign = baker.make(Campaign)
        authenticate()
        response = api_client.get(f"/campaign/campaigns/0/")
        assert response.status_code == status.HTTP_404_NOT_FOUND
    
    def test_if_anonymous_returns_401(self, api_client, authenticate):
        campaign = baker.make(Campaign)
        response = api_client.get(f"/campaign/campaigns/{campaign.pk}/")
        assert response.status_code == status.HTTP_401_UNAUTHORIZED

