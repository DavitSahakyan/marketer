from rest_framework import status
import pytest
from model_bakery import baker
from campaign.models import Contact, CSVFile
from django.core.files.uploadedfile import SimpleUploadedFile

@pytest.fixture
def create_csvfile(api_client):
    def do_create_csvfile(contact):
       return api_client.post('/campaign/csvfiles/', contact)
    return do_create_csvfile

@pytest.mark.django_db
class TestCSVFileCreate:
    def test_if_anonymous_returns_401(self, create_csvfile):
        CSVFile.file_field = SimpleUploadedFile('test_users.csv', b'these are users for test')
        data = {
            "file_path": CSVFile.file_field,
        }
        response = create_csvfile(data)
        assert response.status_code == status.HTTP_401_UNAUTHORIZED
    
    def test_if_valid_returns_201(self, create_csvfile, authenticate):
        CSVFile.file_field = SimpleUploadedFile('test_users.csv', b'these are users for test')
        authenticate()
        data = {
            "file_path": CSVFile.file_field
        }
        response = create_csvfile(data)
        assert response.status_code == status.HTTP_201_CREATED

    def test_if_invalid_returns_400(self, create_csvfile, authenticate):
        authenticate()
        data = {
        "file_path": ""
        }
        response = create_csvfile(data)
        assert response.status_code == status.HTTP_400_BAD_REQUEST

@pytest.mark.django_db
class TestCSVFileUpdate:
    def test_if_anonymous_returns_401(self, api_client):
        CSVFile.file_field = SimpleUploadedFile('test_user.csv', b'these are users for test')
        data = {
            "id": 1,
            "file_path": CSVFile.file_field,
            "uploaded_by": "dsah0@gmail.com"
        }
        response = api_client.put(f"/campaign/csvfiles/1/", data)
        assert response.status_code == status.HTTP_401_UNAUTHORIZED
    
    def test_if_valid_returns_200(self, api_client, authenticate,create_csvfile):
        CSVFile.file_field = SimpleUploadedFile('test_users.csv', b'these are users for test')
        csvfile = baker.make(CSVFile)
        authenticate()
        data = {
            "id": csvfile.pk,
            "file_path": CSVFile.file_field
        }
        response = api_client.put(f"/campaign/csvfiles/{csvfile.pk}/", data)
        assert response.status_code == status.HTTP_200_OK
    
    def test_if_dont_exists_returns_404(self, api_client, authenticate):
        CSVFile.file_field = SimpleUploadedFile('test_users.csv', b'these are users for test')
        csvfile = baker.make(CSVFile)
        authenticate()
        data = {
            "id": csvfile.pk,
            "file_path": CSVFile.file_field
        }
        response = api_client.put(f"/campaign/csvfiles/0/", data)
        assert response.status_code == status.HTTP_404_NOT_FOUND
    
    def test_if_invalid_data_returns_400(self, api_client, authenticate):
        CSVFile.file_field = SimpleUploadedFile('test_users.csv', b'these are users for test')
        csvfile = baker.make(CSVFile)
        authenticate()
        data = {
            "id": csvfile.pk,
            "file_path": ""
        }
        response = api_client.put(f"/campaign/csvfiles/{csvfile.pk}/", data)
        assert response.status_code == status.HTTP_400_BAD_REQUEST

@pytest.mark.django_db
class TestCSVFileDelete:
    def test_if_anonymous_returns_401(self, api_client):
        csvfile = baker.make(CSVFile)
        response = api_client.delete(f"/campaign/csvfiles/{csvfile.pk}/")
        assert response.status_code == status.HTTP_401_UNAUTHORIZED
    
    def test_if_valid_returns_204(self, api_client, authenticate):
        csvfile = baker.make(CSVFile)
        authenticate()
        response = api_client.delete(f"/campaign/csvfiles/{csvfile.pk}/")
        assert response.status_code == status.HTTP_204_NO_CONTENT
    
    def test_if_dont_exists_404(self, api_client, authenticate):
        csvfile = baker.make(CSVFile)
        authenticate()
        response = api_client.delete(f"/campaign/csvfiles/0/")
        assert response.status_code == status.HTTP_404_NOT_FOUND

@pytest.mark.django_db
class TestCSVFileRetrieve:
    def test_if_valid_list_returns_200(self, api_client, authenticate):
        authenticate()
        response = api_client.get(f"/campaign/csvfiles/")
        assert response.status_code == status.HTTP_200_OK

    def test_if_valid_detail_returns_200(self, api_client, authenticate):
        csvfile = baker.make(CSVFile)
        authenticate()
        response = api_client.get(f"/campaign/csvfiles/{csvfile.pk}/")
        assert response.status_code == status.HTTP_200_OK

    def test_if_dont_exists_returns_404(self, api_client, authenticate):
        csvfile = baker.make(CSVFile)
        authenticate()
        response = api_client.get(f"/campaign/csvfiles/0/")
        assert response.status_code == status.HTTP_404_NOT_FOUND
    
    def test_if_anonymous_returns_401(self, api_client, authenticate):
        csvfile = baker.make(CSVFile)
        response = api_client.get(f"/campaign/csvfiles/{csvfile.pk}/")
        assert response.status_code == status.HTTP_401_UNAUTHORIZED