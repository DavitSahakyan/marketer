from rest_framework.response import Response
from rest_framework import serializers
from datetime import datetime
from django.utils.timezone import make_aware
from campaign.models import CSVFile, Campaign, Contact, Template
from campaign.services import create_contacts_from_csv
from campaign.tasks import create_contacts_send_email, send_emails_in_campaign_background

class CSVFileSerializer(serializers.ModelSerializer):
    class Meta:
        model = CSVFile
        fields = ("id", "file_path", "uploaded_by", "uploaded_at")
    uploaded_by = serializers.PrimaryKeyRelatedField(read_only=True)
    def create(self, validated_data):
        request = self.context['request']
        user = request.user
        validated_data["uploaded_by"] = user
        return super().create(validated_data)


class ContactSerializer(serializers.ModelSerializer):
    class Meta:
        model = Contact
        fields = ("id", "first_name", "last_name", "job_title", "email", "file",
                  "company_name", )
    


class AddContactsBackgroundSerializer(serializers.ModelSerializer):
    class Meta:
        model = Contact
        fields = ("id", "file", "email")
    
    email = serializers.EmailField(read_only=True)

    def create(self, validated_data):
        request = self.context['request']
        user = request.user
        csv_path=f'media/{validated_data["file"].file_path}'
        file_id=validated_data["file"].id
        create_contacts_send_email(csv_path, file_id, user)
        return validated_data

class TemplateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Template
        fields = ("id", "subject", "content", "first_name")


class CampaignCreateSerializer(serializers.ModelSerializer):
    state = serializers.CharField(read_only=True)
    class Meta:
        model = Campaign
        fields = ("id", "goal", "scheduled_time", "state", "csv_files", "template")
    
class CampaignUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Campaign
        fields = ("id", "goal", "scheduled_time", "state", "csv_files", "template")
    
    def update(self, instance, validated_data):
        request = self.context['request']
        if instance.state == "NS":
            if instance.scheduled_time < make_aware(datetime.now()):
                validated_data["csv_files"] = instance.csv_files.all()
            validated_data["state"] = "NS"
        if instance.state == "CM":
            validated_data["state"] = "CM"
            validated_data["csv_files"] = instance.csv_files.all()
        elif instance.state == "ST":
            state = validated_data["state"]
            if state == "NS" or state == "CM":
                validated_data["state"] = "ST"
            validated_data["csv_files"] = instance.csv_files
        elif instance.state == "PA":
            if instance.scheduled_time < make_aware(datetime.now()):
                validated_data["csv_files"] = instance.csv_files.all()
            if validated_data["state"] == "NS" or validated_data["state"] == "CM":
                validated_data["state"] == "PA"
        return super().update(instance, validated_data)

 