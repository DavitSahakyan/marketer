import csv
from django.db.utils import IntegrityError
from campaign.models import Campaign, Contact, CSVFile


def create_contacts_from_csv(csv_path, file_id):
    with open(csv_path) as csv_file:
        contacts = csv.reader(csv_file, delimiter=',')
        line_count, not_created, created = 0, 0, 0
        for contact in contacts:
            if line_count == 0:
                line_count +=1
                continue
            try:
                Contact.objects.create(
                    first_name=contact[0],
                    last_name=contact[1],
                    email=contact[2],
                    company_name=contact[3],
                    job_title=contact[4],
                    file=CSVFile.objects.get(pk=file_id)
                )
                created += 1
                line_count += 1
            except IntegrityError:
                not_created += 1
                line_count += 1

    return line_count - 1, created, not_created


def find_recipient_emails_from_campaign(campaign: Campaign, csv_files: CSVFile):
    path_list = []
    recipient_list = []
    for file in csv_files:
        path_list.append(file.file_path)
    
    for path in path_list:
        with open(f"media/{str(path)}") as csv_file:
            contacts = csv.reader(csv_file, delimiter=',')
            for contact in contacts:
                if contact[2] != "mail":
                    recipient_list.append(contact[2])
    return recipient_list


def find_recipient_first_names(campaign: Campaign, csv_files: CSVFile):
    path_list = []
    recipient_list = []
    for file in csv_files:
        path_list.append(file.file_path)
    
    for path in path_list:
        with open(f"media/{str(path)}") as csv_file:
            contacts = csv.reader(csv_file, delimiter=',')
            for contact in contacts:
                if contact[0] != "first_name":
                    recipient_list.append(contact[0])
    return recipient_list  
