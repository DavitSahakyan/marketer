from django.urls import path, include
from rest_framework.routers import DefaultRouter 
from campaign.views import CSVFileViewSet, ContactViewSet, \
                           CampaignCreateAPIView, TemplateViewSet, \
                           CampaignUpdateAPIView, AddContactsBackgroundViewSet


router = DefaultRouter()
router.register("csvfiles", CSVFileViewSet)
router.register("contacts", ContactViewSet)
router.register("templates", TemplateViewSet)
router.register("upload", AddContactsBackgroundViewSet)
urlpatterns = [
    path("", include(router.urls)),
    path("campaigns/<int:pk>/", CampaignUpdateAPIView.as_view()),
    path("campaigns/", CampaignCreateAPIView.as_view()),
]